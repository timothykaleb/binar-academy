const dataPenjualanPakAldi = [
    {
      namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
      hargaSatuan: 760000,
      kategori : "Sepatu Sport",
      totalTerjual : 90,
    },
    {
      namaProduct : 'Sepatu Warrior Tristan Black Brown High',
      hargaSatuan: 960000,
      kategori : "Sepatu Sneaker",
      totalTerjual : 37,
    },
    {
      namaProduct : 'Sepatu Warrior Tristan Maroon High ',
      kategori : "Sepatu Sneaker",
      hargaSatuan: 360000,
      totalTerjual : 90,
    },
    {
      namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
      hargaSatuan: 120000,
      kategori : "Sepatu Sneaker",
      totalTerjual : 90,
    }
  ]

function hitungTotalPenjualan(dataPenjualan){
    // console.log(dataPenjualan.length)
    total=0
    if(Array.isArray(dataPenjualan)==true){
        for(i=0; i<dataPenjualan.length;i++){
            data_harga = dataPenjualan[i].hargaSatuan
            data_total_terjual = dataPenjualan[i].totalTerjual
            total_harga = data_harga*data_total_terjual
            total = total+total_harga
        }
        return total
    }
    else{
        return "FORMAT DATA SALAH"
    }

}

console.log(hitungTotalPenjualan(dataPenjualanPakAldi))
console.log(hitungTotalPenjualan("halo"))
const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
/*Total keuntungan*
total modal*
presentaseKeuntungan
produkBukuTerlaris
penulisTerlaris */
function getInfoPenjualan(data){
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
    })
     if(Array.isArray(data)==true){
        console.log("masuk if")
        total_keuntungan = 0
        total_modal = 0
        total_terjual = 0
        nama_buku_terlaris = "DEFAULT"
        nama_penulis_terlaris = "DEFAULT"
        for(i=0; i<data.length;i++){
            data_keuntungan = (data[i].hargaJual-data[i].hargaBeli)*data[i].totalTerjual
            data_modal = data[i].hargaBeli*data[i].totalTerjual
            total_keuntungan = data_keuntungan+total_keuntungan
            // console.log(data[i].hargaBeli*data[i].totalTerjual)
            total_modal = data_modal+total_modal
            presentase_keuntungan = (total_keuntungan/total_modal) *100
            if(data[i].totalTerjual > total_terjual){
                nama_buku_terlaris = data[i].namaProduk
                nama_penulis_terlaris = data[i].penulis
                total_terjual = data[i].totalTerjual
            }
        }
        return  {
                TotalKeuntungan: formatter.format(total_keuntungan),
                TotalModal : formatter.format(total_modal),
                presentaseKeuntungan: parseFloat(presentase_keuntungan).toFixed(2)+"%",
                produkBukuTerlaris : nama_buku_terlaris,
                namaPenulisTerlaris : nama_penulis_terlaris
              }
     }
    else{
        return "FORMAT DATA SALAH"
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel))
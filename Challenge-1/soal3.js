function checkEmail(email){
    if(typeof email=="string"){
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(mailformat)){
            return "VALID"
        }
        else{
            return "INVALID"
        }
    }
    else{
        return "INVALID"
    }
}

console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))
console.log(checkEmail('aaaaa'))
console.log(checkEmail(0))

function getAngkaTerbesarKedua(number){
    biggest = 0
    second = 0
    // console.log(number.length)
    if(!number){
        return "ERROR ANGKA BUKAN ARRAY"
    }
    else{
        for(let i=0; i<number.length; i++){
            // console.log(i)
            if(number[i]>biggest){
                // console.log(number[i])

                second = biggest
                biggest = number[i]
            }
            else if(number[i]!=biggest && number[i]>second){
                second = number[i]
            }
        }
        return second
    }
}
const dataAngka = [9,9,9,9,9,4,5]

console.log(getAngkaTerbesarKedua(dataAngka))
// console.log(getAngkaTerbesarKedua(0))
// console.log(getAngkaTerbesarKedua())